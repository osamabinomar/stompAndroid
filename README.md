# STOMP protocol via WebSocket for Android

[![](https://jitpack.io/v/com.gitlab.osamabinomar.osamabinomar/stompAndroid.svg)](https://jitpack.io/#com.gitlab.osamabinomar.osamabinomar/stompAndroid)

## Overview

This library provide support for STOMP protocol https://stomp.github.io/
At now library works only as client for backend with support STOMP, such as
NodeJS (stompjs or other) or Spring Boot (SockJS).

Add library as gradle dependency

```gradle
repositories { 
    jcenter()
    maven { url "https://jitpack.io" }
}
dependencies {
    implementation 'com.gitlab.osamabinomar.osamabinomar:stompAndroid:1.0'
}
```

## Example library usage

**Basic usage**
``` java
 import org.java_websocket.WebSocket;

 private StompClient mStompClient;
 
 // ...
 
 mStompClient = Stomp.over(WebSocket.class, "ws://10.0.2.2:8080/example-endpoint/websocket");
 mStompClient.connect();
  
 mStompClient.topic("/topic/greetings").subscribe(topicMessage -> {
     Log.d(TAG, topicMessage.getPayload());
 });
  
 mStompClient.send("/topic/hello-msg-mapping", "My first STOMP message!").subscribe();
  
 // ...
 
 mStompClient.disconnect();

```


Method `Stomp.over` consume class for create connection as first parameter.
You must provide dependency for lib and pass class.
At now supported connection providers:
- `org.java_websocket.WebSocket.class` ('org.java-websocket:Java-WebSocket:1.3.0')
- `okhttp3.WebSocket.class` ('com.squareup.okhttp3:okhttp:3.8.0')

You can add own connection provider. Just implement interface `ConnectionProvider`.
If you implement new provider, please create pull request :)

**Subscribe lifecycle connection**
``` java
mStompClient.lifecycle().subscribe(lifecycleEvent -> {
    switch (lifecycleEvent.getType()) {
    
        case OPENED:
            Log.d(TAG, "Stomp connection opened");
            break;
            
        case ERROR:
            Log.e(TAG, "Error", lifecycleEvent.getException());
            break;
            
        case CLOSED:
             Log.d(TAG, "Stomp connection closed");
             break;
    }
});
```

Library support just send & receive messages. ACK messages, transactions not implemented yet.
All credit goes to **https://github.com/NaikSoftware/StompProtocolAndroid**
